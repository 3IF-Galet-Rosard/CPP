#ifndef MAIN_CPP
#define MAIN_CPP

using namespace std;
#include <iostream>
#include "Catalogue.h"

int main() {
    Catalogue* ctl = new Catalogue;
    int choix = -1;

    while (choix != 0) {
        choix = ctl->Menu();

        switch (choix){
            case 1:
                ctl->NouveauTrajet();
                break;
            case 2:
                ctl->Afficher();
                break;
            case 3:
                ctl->Rechercher();
                break;
            case 4:
                ctl->Sauvegarder();
                break;
            case 5:
                ctl->Charger();
                break;
        }

        cout << "-----------"<< endl;
    }
    delete ctl;
    return 0;
}

#endif
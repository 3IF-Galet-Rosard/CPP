/*************************************************************************
                           Trajet  -  description
                             -------------------
    début                : 21/11/2022
    copyright            : (C) 2022 par Arthur Galet, Alexandre Rosard
    e-mail               : arthur.galet@insa-lyon.fr, alexandre.rosard@insa-lyon.fr
*************************************************************************/

//---------- Interface de la classe Trajet (fichier Trajet.h) ----------------
#if ! defined ( Trajet_H )
#define Trajet_H
#include <fstream>

//------------------------------------------------------------------------
// Rôle de la classe Trajet
// classe abstraite pour l'utilisation dans un Element d'une liste chainee
// les classes TrajetSimple et TrajetCompose en hérite
//------------------------------------------------------------------------

using namespace std;

class Trajet
{
//----------------------------------------------------------------- PUBLIC
public:
//----------------------------------------------------- Méthodes publiques

    virtual const char* ObtenirDepart() const = 0;

    virtual const char* ObtenirArrivee() const = 0;

    virtual bool EstCompose() const = 0;

    virtual void Afficher() const = 0;

    virtual void Sauvegarder(ofstream & out) const = 0;

//-------------------------------------------- Constructeurs - destructeur
    Trajet(const Trajet & unTrajet);
    // Contrat :
    // pas d'utilisation, utile pour debuggage

    Trajet();

    virtual ~Trajet ( );

};

#endif // Trajet_H
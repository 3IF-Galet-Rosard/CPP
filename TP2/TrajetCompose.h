/*************************************************************************
                           TrajetCompose  -  description
                             -------------------
    début                : 28/11/2022
    copyright            : Arthur Galet, Alexandre Rosard
    e-mail               : arthur.galet@insa-lyon.fr, alexandre.rosard@insa-lyon.fr
*************************************************************************/

//---------- Interface de la classe TrajetCompose (fichier TrajetCompose.h) ----------------
#if ! defined ( TrajetCompose_H )
#define TrajetCompose_H

//--------------------------------------------------- Interfaces utilisées
#include "ListeChainee.h"

//------------------------------------------------------------------------
// Rôle de la classe TrajetCompose
// modélise un trajet compose de plusieurs etapes qui sont des trajets
// simple stockés dans une liste chainee
//------------------------------------------------------------------------

class TrajetCompose: public Trajet
{
//----------------------------------------------------------------- PUBLIC

public:
//----------------------------------------------------- Méthodes publiques
    virtual const char* ObtenirDepart() const;
    // Mode d'emploi :
    // retourne le depart de la premiere etape du trajet compose

    virtual const char* ObtenirArrivee() const;
    // Mode d'emploi :
    // retourne l'arrivee de la derniere etape du trajet compose

    virtual bool EstCompose() const;
    // Mode d'emploi :
    // renvoie vrai car le Trajet est un TrajetCompose
    // Contrat :
    // pas d'utilisation en dehors du filtre de sauvegarde TrajetSimple/TrajetCompose

    virtual void Afficher() const;
    // Mode d'emploi :
    // affiche chaque etape du trajet

    void Sauvegarder (ofstream & out) const;
    // Mode d'emploi :
    // renvoie le trajet compose au format de sauvegarde
    // dans le flux de sortie passe en parametre

    //-------------------------------------------- Constructeurs - destructeur
    TrajetCompose(const TrajetCompose & unTrajetCompose);
    // Contrat :
    // pas d'utilisation, utile pour debuggage

    TrajetCompose(ListeChainee* voy);
    // Mode d'emploi :
    // construit un trajet compose dont la liste chainee est passee en parametre

    virtual ~TrajetCompose();

//------------------------------------------------------------------ PRIVE
protected:
//----------------------------------------------------- Attributs protégés
    ListeChainee* voyage;
};

#endif // TrajetCompose_H
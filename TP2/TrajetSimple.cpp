/*************************************************************************
                           TrajetSimple  -  description
                             -------------------
    début                : 21/11/2022
    copyright            : (C) 2022 par Arthur Galet, Alexandre Rosard
    e-mail               : arthur.galet@insa-lyon.fr, alexandre.rosard@insa-lyon.fr
*************************************************************************/

//---------- Réalisation de la classe TrajetSimple (fichier TrajetSimple.cpp) ------------

//---------------------------------------------------------------- INCLUDE
//-------------------------------------------------------- Include système
//------------------------------------------------------ Include personnel
#include "TrajetSimple.h"

//----------------------------------------------------------------- PUBLIC
//----------------------------------------------------- Méthodes publiques
const char* TrajetSimple::ObtenirDepart() const
{
    return depart;
} //----- Fin de ObtenirDepart

const char* TrajetSimple::ObtenirArrivee() const
{
    return arrivee;
} //----- Fin de ObtenirArrivee

bool TrajetSimple::EstCompose() const
{
    return false;
} //----- Fin de EstCompose

void TrajetSimple::Afficher() const
{
    cout << "Trajet " << depart << " - " << arrivee << " en " << moyenTransport;
} //----- Fin de Afficher

void TrajetSimple::Sauvegarder(ofstream & out) const
{
    out << depart << "," << arrivee << "," << moyenTransport << endl;
} //----- Fin de Sauvegarder

//-------------------------------------------- Constructeurs - destructeur
TrajetSimple::TrajetSimple(const TrajetSimple & tjs)
{
#ifdef MAP
    cout << "Appel au constructeur de copie de TrajetSimple" << endl;
#endif

} //----- Fin de TrajetSimple (constructeur de copie)

TrajetSimple::TrajetSimple(const char* dep, const char* arr, const char* mtp)
{
#ifdef MAP
    cout << "Appel au constructeur de TrajetSimple" << endl;
#endif
    depart = new char[TAILLE_CHAINE];
    arrivee = new char[TAILLE_CHAINE];
    moyenTransport = new char[TAILLE_CHAINE];

    strcpy(depart, dep);
    strcpy(arrivee, arr);
    strcpy(moyenTransport, mtp);
} //----- Fin de TrajetSimple


TrajetSimple::~TrajetSimple()
{
#ifdef MAP
    cout << "Appel au destructeur de TrajetSimple" << endl;
#endif
    delete[] depart;
    delete[] arrivee;
    delete[] moyenTransport;
} //----- Fin de ~TrajetSimple
/*************************************************************************
                           TrajetSimple  -  description
                             -------------------
    début                : 21/11/2022
    copyright            : (C) 2022 par Arthur Galet, Alexandre Rosard
    e-mail               : arthur.galet@insa-lyon.fr, alexandre.rosard@insa-lyon.fr
*************************************************************************/

//---------- Interface de la classe TrajetSimple (fichier TrajetSimple.h) ----------------
#if ! defined ( TRAJETSIMPLE_H )
#define TRAJETSIMPLE_H

//--------------------------------------------------- Interfaces utilisées
#include "Trajet.h"
#include <string>
#include <iostream>
#include <cstring>

//------------------------------------------------------------------------
// Rôle de la classe TrajetSimple
// modélisation d'un trajet entre deux villes qui utilise un moyen de
// transport défini
//------------------------------------------------------------------------
using namespace std;

class TrajetSimple : public Trajet
{

//----------------------------------------------------------------- PUBLIC
public:
//----------------------------------------------------- Méthodes publiques

    virtual const char* ObtenirDepart() const;
    // Mode d'emploi :
    // retourne le depart du trajet

    virtual const char* ObtenirArrivee() const;
    // Mode d'emploi :
    // retourne l'arrivee du trajet

    virtual bool EstCompose() const;
    // Mode d'emploi :
    // renvoie faux car le Trajet n'est pas un TrajetCompose
    // Contrat :
    // pas d'utilisation en dehors du filtre de sauvegarde TrajetSimple/TrajetCompose

    void Afficher() const;
    // Mode d'emploi :
    // affiche les caracteristiques du trajet

    virtual void Sauvegarder(ofstream & out) const;
    // Mode d'emploi :
    // renvoie le trajet simple au format de sauvegarde
    // dans le flux de sortie passe en parametre

//-------------------------------------------- Constructeurs - destructeur
    TrajetSimple(const TrajetSimple & tjs);
    // Contrat :
    // pas d'utilisation, utile pour debuggage

    TrajetSimple(const char* depart, const char* arrivee, const char* mtp);
    // Mode d'emploi :
    // construit un trajet simple avec les caracteristiques passees en parametres
    // Contrat :
    // les chaines de caractères doivent être limitées à 40 caractères (41 avec \0)

    virtual ~TrajetSimple();

//------------------------------------------------------------------ PRIVE

protected:
//----------------------------------------------------- Attributs protégés
    static const int TAILLE_CHAINE = 41;
    char* depart;
    char* arrivee;
    char* moyenTransport;
};
#endif // TRAJETSIMPLE_H
/*************************************************************************
                           TrajetCompose  -  description
                             -------------------
    début                : 28/11/2022
    copyright            : Arthur Galet, Alexandre Rosard
    e-mail               : arthur.galet@insa-lyon.fr, alexandre.rosard@insa-lyon.fr
*************************************************************************/

//---------- Réalisation de la classe <TrajetCompose> (fichier TrajetCompose.cpp) ------------

//---------------------------------------------------------------- INCLUDE
//-------------------------------------------------------- Include système
#include <iostream>

using namespace std;
//------------------------------------------------------ Include personnel
#include "TrajetCompose.h"


//----------------------------------------------------------------- PUBLIC
//----------------------------------------------------- Méthodes publiques
const char* TrajetCompose::ObtenirDepart() const
{
    return voyage->ObtenirTete()->ObtenirTrajet()->ObtenirDepart();
} //----- Fin de Méthode ObtenirDépart


const char* TrajetCompose::ObtenirArrivee() const
{
    return voyage->ObtenirQueue()->ObtenirTrajet()->ObtenirArrivee();
} //----- Fin de Méthode ObtenirArrivée

bool TrajetCompose::EstCompose() const
{
    return true;
} //----- Fin de EstCompose

void TrajetCompose::Afficher() const
{
    Element* partieTrajetCompose = voyage->ObtenirTete();
    while(partieTrajetCompose != nullptr){
        partieTrajetCompose->ObtenirTrajet()->Afficher();
        cout << " | ";
        partieTrajetCompose = partieTrajetCompose->ObtenirSuivant();
    }
} //----- Fin de Méthode Afficher

void TrajetCompose::Sauvegarder (ofstream & out) const
{
    Element* elem = voyage->ObtenirTete();

    out << "-----" << endl;
    while (elem != nullptr) {
        elem->ObtenirTrajet()->Sauvegarder(out);
        elem = elem->ObtenirSuivant();
    }
    out << "-----" << endl;

} //----- Fin de Sauvegarder

//-------------------------------------------- Constructeurs - destructeur
TrajetCompose::TrajetCompose(const TrajetCompose & unTrajetCompose)
{
#ifdef MAP
    cout << "Appel au constructeur de copie de TrajetCompose" << endl;
#endif
} //----- Fin de TrajetCompose (constructeur de copie)

TrajetCompose::TrajetCompose(ListeChainee* voy)
{
#ifdef MAP
    cout << "Appel au constructeur de TrajetCompose" << endl;
#endif
    voyage = voy;
} //----- Fin de TrajetCompose

TrajetCompose::~TrajetCompose()
{
#ifdef MAP
    cout << "Appel au destructeur de TrajetCompose" << endl;
#endif
    delete voyage;
} //----- Fin de ~TrajetCompose
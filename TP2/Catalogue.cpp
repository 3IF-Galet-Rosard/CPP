/*************************************************************************
                           Catalogue  -  description
                             -------------------
    début                : 21/11/2022
    copyright            : (C) 2022 par Arthur Galet, Alexandre Rosard
    e-mail               : arthur.galet@insa-lyon.fr, alexandre.rosard@insa-lyon.fr
*************************************************************************/

//---------- Réalisation de la classe Catalogue (fichier Catalogue.cpp) ------------

//---------------------------------------------------------------- INCLUDE
//-------------------------------------------------------- Include système
using namespace std;
#include <iostream>
#include <cstring>

//------------------------------------------------------ Include personnel
#include "Catalogue.h"
#include "ListeChainee.h"
#include "TrajetSimple.h"
#include "TrajetCompose.h"

//----------------------------------------------------------------- PUBLIC
//----------------------------------------------------- Méthodes publiques
int Catalogue::Menu() const
{
    int choix = -1;
    cout << "(1) Creer et ajouter un nouveau trajet" << endl;
    cout << "(2) Afficher le catalogue" << endl;
    cout << "(3) Rechercher un trajet" << endl;
    cout << "(4) Sauvegarder des trajets" << endl;
    cout << "(5) Charger les trajets sauvegardes" << endl;
    cout << "(0) Quitter le menu" << endl;
    cin >> choix;

    // saisie invalide
    while (choix < 0 or choix > 5) {
        cout << "Choix invalide, veuillez recommencer : " << endl;
        cin >> choix;
    }

    return choix;
} //----- Fin de Menu


void Catalogue::NouveauTrajet()
{
    char* dep = new char[TAILLE_CHAINE];
    char* arr = new char[TAILLE_CHAINE];
    char* mtp = new char[TAILLE_CHAINE];
    int continuer = 1;
    TrajetSimple* trj;
    Element* elem;

    cout << "Entrer le depart du trajet : ";
    cin >> dep;
    cout << "Entrer l'arrivee du trajet : ";
    cin >> arr;
    cout << "Entrer le moyen de transport : ";
    cin >> mtp;
    cout << "Ajouter une etape supplementaire ? (1 si oui, 0 si non) : ";
    cin >> continuer;

    if (continuer == 1){ // Trajet Compose
        ListeChainee* voyage = new ListeChainee();
        Element* etape;

        // boucle de lecture des etapes du trajet compose
        while(continuer == 1) {
            // creation d'un element correspondant a la lecture precedente
            trj = new TrajetSimple(dep, arr, mtp);
            etape = new Element(trj);
            voyage->InsererFin(etape);
            // Le depart de la prochain etape est l'arrivee de la precedente (coherence du trajet compose)
            strcpy(dep,arr);

            cout << "Entrer l'arrivee du trajet : ";
            cin >> arr;
            cout << "Entrer le moyen de transport: ";
            cin >> mtp;
            cout << "Ajouter une etape supplementaire ? (1 si oui, 0 si non) : ";
            cin >> continuer;
        }
        cout << endl;

        // creation dernier element
        trj = new TrajetSimple(dep, arr, mtp);
        etape = new Element(trj);
        voyage->InsererFin(etape);
        TrajetCompose* trjc = new TrajetCompose(voyage);
        elem = new Element(trjc);

    } else { // Trajet Simple
        cout <<endl;
        trj = new TrajetSimple(dep, arr, mtp);
        elem = new Element(trj);
    }

    insereTrajet(elem);

    delete[] dep;
    delete[] arr,
    delete[] mtp;
} //----- Fin de NouveauTrajet

void Catalogue::Afficher() const
// Algorithme :
// parcours de la liste avec affichage du rang et du trajet pour chaque element
{
    Element* elem = liste->ObtenirTete();
    int rang = 1;

    while (elem != nullptr) {
        cout << rang++ << ": ";
        elem->ObtenirTrajet()->Afficher();
        cout << endl;
        elem = elem->ObtenirSuivant();
    }
} //----- Fin de Afficher

void Catalogue::Rechercher() const
// Algorithme :
// parcours de la liste chainee avec affichage des trajets dont le depart
// et l'arrivee correspondent aux informations saisies par l'utilisateur
// recherche avancee : construction recursive de la liste de tout les
// parcours possibles depuis les sommets en utilisant un parcours de graphe
{
    char* dep = new char[TAILLE_CHAINE];
    char* arr = new char[TAILLE_CHAINE];
    bool trouve = false;

    // lecture clavier
    cout << "Rechercher un trajet partant de : ";
    cin >> dep;
    cout << "Trajet arrivant a : ";
    cin >> arr;

    // recherche simple
    cout << "Resultats - trajets atomiques :" << endl;

    // parcours de la liste des trajets
    Element* elem = liste->ObtenirTete();
    while(elem != NULL) {
        // test de correspondance
        if (strcmp(elem->ObtenirTrajet()->ObtenirArrivee(), arr) == 0
        and strcmp(elem->ObtenirTrajet()->ObtenirDepart(), dep) == 0) {
            elem->ObtenirTrajet()->Afficher();
            cout << endl;
            trouve = true;
        }

        elem = elem->ObtenirSuivant();
    }

    if (!trouve) { // message en cas d'absence de resultat
        cout << "Aucun trajet correspondant" << endl;
    }

    // recherche avancee

    // algorithme de parcours de graphe en profondeur
    ListeChainee* marque = new ListeChainee(); // sommets/element marques lors du parcours
    Element* elemMarque;
    bool estMarque;
    elem = liste->ObtenirTete();

    while(elem != nullptr) {
        // test de presence dans la liste des elements marques
        estMarque = false;
        elemMarque = marque->ObtenirTete();

        while(not estMarque and elemMarque != nullptr) {
            // test d'egalite
            estMarque = strcmp(elem->ObtenirTrajet()->ObtenirDepart(), elemMarque->ObtenirTrajet()->ObtenirDepart()) == 0
                and strcmp(elem->ObtenirTrajet()->ObtenirArrivee(), elemMarque->ObtenirTrajet()->ObtenirArrivee()) == 0;
            elemMarque = elemMarque->ObtenirSuivant();
        }

        if (not estMarque) {
            ListeChainee* parcours = new ListeChainee(); // liste pour affichage des resultats de la recherche
            parcours->InsererDebut(new Element(elem->ObtenirTrajet()));

            // debut de recursion pour le sommet/trajet elem
            rechercheAvancee(marque, parcours, arr);

            // suppresion de la liste cree pour ce parcours
            delete parcours->ObtenirTete();
            parcours->MettreTete(nullptr);
            delete parcours;
        }

        elem = elem->ObtenirSuivant();
    }

    // destruction de marque sans detruire les trajets contenus - uniquement les elements
    elemMarque = marque->ObtenirTete();
    while(elemMarque != nullptr) {
        elem = elemMarque;
        elemMarque = elemMarque->ObtenirSuivant();
        delete elem;
    }

    marque->MettreTete(nullptr);
    delete marque;
    delete[] dep;
    delete[] arr;
} //----- Fin de Rechercher

void Catalogue::Sauvegarder() const
// Algorithme :
// saisie des criteres de sauvegarde puis parcours de la liste chainee
// tout les criteres sont evalues pour chaque trajet
{
    bool choixDepart,choixArrivee,choixIntervalle,choixType,filtreCompose;
    int rangMin, rangMax;
    string depart,arrivee,fichier;
    ofstream ofs;

    cout << "Entrez le nom du fichier de sauvegarde : ";
    cin >> fichier;
    ofs.open(fichier, ios_base::out);
    while (ofs.fail()) {
        cout << "Erreur de fichier, veuillez entrer un autre nom : ";
        cin.clear();
        cin >> fichier;
        ofs.open(fichier, ios_base::out);
    }

    cout << "Voulez-vous affiner votre sauvegarde par Depart ? (1:Oui/0:Non) : ";
    choixDepart = saisieEntier();
    if (choixDepart){
        cout << "Choisissez le depart : ";
        cin >> depart;
    }

    cout << "Voulez-vous affiner votre sauvegarde par Arrivee ? (1:Oui/0:Non) : ";
    choixArrivee = saisieEntier();
    if (choixArrivee){
        cout << "Choisissez l'arrivee : ";
        cin >> arrivee;
    }

    cout << "Voulez-vous ne sauvegarder qu'un intervalle de trajets ? (1:Oui/0:Non) : ";
    choixIntervalle = saisieEntier();
    if (choixIntervalle){
        cout << "Choisissez le numero minimum : ";
        rangMin = saisieEntier();
        cout << "Choisissez le numero maximum : ";
        rangMax = saisieEntier();
    }

    cout << "Voulez-vous affiner votre sauvegarde par type de Trajet ? (1:Oui/0:Non) : ";
    choixType = saisieEntier();
    if (choixType){
        cout << "Choisissez le filtreCompose de trajet a conserver (1:TrajetComposes/0:TrajetsSimple): ";
        filtreCompose = saisieEntier();
    }

    Element* elem = liste->ObtenirTete();
    int rang = 1;

    while (elem != nullptr) {
        if ((not choixIntervalle or (rang >= rangMin && rang <= rangMax))
            and (not choixDepart or (depart == elem->ObtenirTrajet()->ObtenirDepart()))
            and (not choixArrivee or (arrivee == elem->ObtenirTrajet()->ObtenirArrivee()))
            and (not choixType
                or (not filtreCompose and not elem->ObtenirTrajet()->EstCompose())
                or (filtreCompose and elem->ObtenirTrajet()->EstCompose()))
            ) {
                elem->ObtenirTrajet()->Sauvegarder(ofs);
        }
        elem = elem->ObtenirSuivant();
        rang++;
    }
    ofs.close();
} //----- Fin de Sauvegarder

void Catalogue::Charger()
// Algorithme :
// saisie des criteres de chargement puis parcours du fichier de sauvegarde
// criteres d'intervalle/type de trajets => les lignes invalides sont passees
// criteres de départ/arrivee => les lignes sont lues mais ne sont pas ajoutees
{
    bool choixDepart,choixArrivee,choixIntervalle,choixType,filtreCompose;
    int rangMin, rangMax;
    string depart,arrivee,fichier;
    ifstream ifs;

    cout << "Entrez le nom du fichier a charger : ";
    cin >> fichier;
    ifs.open(fichier, ios_base::in);
    while (ifs.fail()) {
        cout << "Erreur de fichier, veuillez entrer un autre nom : ";
        cin.clear();
        cin >> fichier;
        ifs.open(fichier, ios_base::in);
    }

    cout << "Voulez-vous affiner votre chargement par Depart ? (1:Oui/0:Non) : ";
    choixDepart = saisieEntier();
    if (choixDepart){
        cout << "Choisissez le depart : ";
        cin >> depart;
    }

    cout << "Voulez-vous affiner votre chargement par Arrivee ? (1:Oui/0:Non) : ";
    choixArrivee = saisieEntier();
    if (choixArrivee){
        cout << "Choisissez l'arrivee : ";
        cin >> arrivee;
    }

    cout << "Voulez-vous ne charger qu'un intervalle de trajets ? (1:Oui/0:Non) : ";
    choixIntervalle = saisieEntier();
    if (choixIntervalle){
        cout << "Choisissez le numero minimum : ";
        rangMin = saisieEntier();
        cout << "Choisissez le numero maximum : ";
        rangMax = saisieEntier();
    }

    cout << "Voulez-vous affiner votre chargement par type de Trajet ? (1:Oui/0:Non) : ";
    choixType = saisieEntier();
    if (choixType){
        cout << "Choisissez le type de trajet a conserver (1:TrajetComposes/0:TrajetsSimple): ";
        filtreCompose = saisieEntier();
    }

    string ligne,mot,trajet[3];
    int i, rangTrajet = 0;
    bool estCompose = false;
    ListeChainee* listeTrajetCompose;

    // parcours du fichier
    while (getline(ifs,ligne)) {

        if (ligne == "-----") { // ligne : delimiteur de trajet compose

            if (estCompose) { // fin du TrajetCompose
                if ((not choixDepart or trajet[1] == depart)
                    and (not choixArrivee or trajet[2] == arrivee)
                    and (not choixType or filtreCompose))
                {
                        insereTrajet(new Element(new TrajetCompose(listeTrajetCompose)));
                }

            } else { // debut d'un TrajetCompose
                ++rangTrajet;
                if ((choixIntervalle and (rangTrajet < rangMin or rangTrajet > rangMax))
                    or (choixType and not filtreCompose)) {
                    // ignorer les TrajetComposes si hors intervalle
                    // ou chargement restreint aux TrajetSimples
                    estCompose = not(estCompose);
                    continue;

                } else {
                    listeTrajetCompose = new ListeChainee;
                }
            }
            estCompose = not(estCompose);

        } else { // ligne : trajet
            if (not estCompose) {
                // pas d'incrementation du rang pour les etapes de TrajetComposes
                ++rangTrajet;
            }

            if ((choixIntervalle and (rangTrajet < rangMin or rangTrajet > rangMax))
                or (choixType and estCompose and not filtreCompose)
                or (choixType and not estCompose and filtreCompose))
            {
                // ignorer TrajetSimple ou etapes de TrajetComposes hors intervalle
                // ou etapes de TrajetComposes lors d'un chargement restreint aux TrajetSimples
                // ou TrajetSimples lors d'un chargement restreint aux TrajetComposes
                continue;
            }

            istringstream iss(ligne);
            i = 0;
            while (getline(iss, mot, ',')) {
                trajet[i] = mot;
                ++i;
            }

            // le TrajetSimple fait partie d'un TrajetCompose
            if (estCompose) {
                listeTrajetCompose->InsererFin(
                        new Element(
                                new TrajetSimple(trajet[0].c_str(), trajet[1].c_str(), trajet[2].c_str())));

            // le TrajetSimple ne fait pas partie d'un TrajetCompose
            // vérification de la correspondance aux criteres de selections
            } else if ((not choixDepart or trajet[1] == depart)
                and (not choixArrivee or trajet[2] == arrivee)) {

                // le TrajetSimple est ajoute au Catalogue.
                insereTrajet(new Element(
                        new TrajetSimple(trajet[0].c_str(), trajet[1].c_str(), trajet[2].c_str())));
            }
        }
    }

    ifs.close();
} //----- Fin de Charger

//-------------------------------------------- Constructeurs - destructeur
Catalogue::Catalogue(const Catalogue & ctl)
{
#ifdef MAP
    cout << "Appel au constructeur de copie de Catalogue" << endl;
#endif
} //----- Fin de Catalogue (constructeur de copie)

Catalogue::Catalogue()
{
#ifdef MAP
    cout << "Appel au constructeur de Catalogue" << endl;
#endif
    liste = new ListeChainee;
} //----- Fin de Catalogue


Catalogue::~Catalogue()
{
#ifdef MAP
    cout << "Appel au destructeur de Catalogue" << endl;
#endif
    delete liste;
} //----- Fin de ~Catalogue

void Catalogue::rechercheAvancee(ListeChainee* marque, ListeChainee* parcours, char* arrivee) const
// Algorithme :
// parcours de graphe en profondeur en prenant les elements comme sommets
// et les correspondances arrivee->depart comme aretes
{
    Element* elem = liste->ObtenirTete();
    Element* elemMarque;
    bool estMarque;

    // fin de récursion : fin du parcours egal a l'arrivee attendue
    if (strcmp(parcours->ObtenirQueue()->ObtenirTrajet()->ObtenirArrivee(), arrivee) == 0) {

        // pas d'affichage si le trajet est atomique
        if (parcours->ObtenirTete() != parcours->ObtenirQueue()) {

            // affichage de la solution
            cout << "Solution - recherche avancee : " << endl;
            elem = parcours->ObtenirTete();
            while (elem != nullptr) {
                elem->ObtenirTrajet()->Afficher();
                cout << endl;
                elem = elem->ObtenirSuivant();
            }
        }

        return;
    }

    // poursuite de la récursion
    // marquage du dernier element ajoute au parcours
    marque->InsererFin(new Element(parcours->ObtenirQueue()->ObtenirTrajet()));

    // parcours de chaque trajet du catalogue
    while(elem != nullptr) {
        // on ne continue que si le debut du trajet courant correspond a l'arrivee du parcours actuel
        if (strcmp(parcours->ObtenirQueue()->ObtenirTrajet()->ObtenirArrivee(), elem->ObtenirTrajet()->ObtenirDepart()) != 0) {
            elem = elem->ObtenirSuivant();
            continue;
        }

        // on ne continue que si le trajet courant n'est pas deja marque
        estMarque = false;
        elemMarque = marque->ObtenirTete();
        // parcours des trajets marques
        while (not estMarque and elemMarque != nullptr) {
            // test d'egalite entre le trajet courant et le trajet marque actuel
            estMarque = strcmp(elem->ObtenirTrajet()->ObtenirDepart(), elemMarque->ObtenirTrajet()->ObtenirDepart()) == 0
                and strcmp(elem->ObtenirTrajet()->ObtenirArrivee(), elemMarque->ObtenirTrajet()->ObtenirArrivee()) == 0;
            elemMarque = elemMarque->ObtenirSuivant();
        }

        // trajet non marque + correspondance arrivee-depart
        // => valide pour continuer la recursion
        if (not estMarque) {
            // ajout du trajet courant au parcours
            parcours->InsererFin(new Element(elem->ObtenirTrajet()));

            // lancement de la prochaine recursinon
            rechercheAvancee(marque, parcours, arrivee);

            // suppresion du dernier trajet du parcours une fois termine
            elemMarque = parcours->ObtenirTete();
            while(elemMarque->ObtenirSuivant() != parcours->ObtenirQueue()) {
                elemMarque = elemMarque->ObtenirSuivant();
            }

            parcours->MettreQueue(elemMarque);
            delete elemMarque->ObtenirSuivant();
            elemMarque->MettreSuivant(nullptr);
        }

        elem = elem->ObtenirSuivant();
    }
} //----- Fin de rechercheAvancee

void Catalogue::insereTrajet(Element* elem)
// Algorithme :
// simple iteration d'un tri bulle
// le trajet est insere au debut de la liste chainee puis compare avec le trajet suivant
// si ces deux trajets sont mal positionnes l'un par rapport a l'autre, ils sont permutes
{
    // insertion dans le catalogue
    liste->InsererDebut(elem);

    // positionnement dans la liste triee
    Element* precedant = nullptr;

    // condition (NON (fin de liste) ET ((departSvt < departAct)
    // OU (departSvt = departAct ET arriveeSvt < arriveeAct))
    while(elem->ObtenirSuivant() != nullptr
          // departSvt < departAct
          and (strcmp(elem->ObtenirTrajet()->ObtenirDepart(),
                      elem->ObtenirSuivant()->ObtenirTrajet()->ObtenirDepart()) > 0 or
               // departSvt = departAct ET arriveeSvt < arriveeAct
               (strcmp(elem->ObtenirTrajet()->ObtenirDepart(),
                       elem->ObtenirSuivant()->ObtenirTrajet()->ObtenirDepart()) == 0 and
                strcmp(elem->ObtenirTrajet()->ObtenirArrivee(),
                       elem->ObtenirSuivant()->ObtenirTrajet()->ObtenirArrivee()) > 0)))
    {
        //permutation
        Element* suivant = elem->ObtenirSuivant();
        elem->MettreSuivant(suivant->ObtenirSuivant());
        suivant->MettreSuivant(elem);

        if (precedant == nullptr) { // debut de liste
            liste->MettreTete(suivant);
        } else {
            precedant->MettreSuivant(suivant);
        }
        precedant = suivant;
    }

    if (elem->ObtenirSuivant() == nullptr) { // tri arrive a la fin de la liste
        liste->MettreQueue(elem);
    }
} //----- Fin de insereTrajet

int Catalogue::saisieEntier() const
{
    int val;

    cin >> val;
    while (cin.fail()) {
        cout << "Saisie invalide, veuillez recommencer : ";
        cin.clear(); // enlever le statut d'erreur
        cin.ignore(1000, '\n'); // vider le buffer (1000 caractères ou jusqu'à une fin de ligne)
        cin >> val;
    }

    return val;
} //----- Fin de saisieEntier

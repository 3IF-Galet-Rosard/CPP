/*************************************************************************
                           Catalogue  -  description
                             -------------------
    début                : 21/11/2022
    copyright            : (C) 2022 par Arthur Galet, Alexandre Rosard
    e-mail               : arthur.galet@insa-lyon.fr, alexandre.rosard@insa-lyon.fr
*************************************************************************/

//---------- Interface de la classe Catalogue (fichier Catalogue.h) ----------------
#if ! defined ( CATALOGUE_H )
#define CATALOGUE_H
#include <fstream>
#include <string>
#include <sstream>

//------------------------------------------------------------------------
// Rôle de la classe Catalogue
// enregistre la liste des trajets crees par l'utilisateurs et permet à
// l'utilisateur d'effectuer des operations sur cette liste via un menu
//------------------------------------------------------------------------

#include "ListeChainee.h"

class Catalogue
{
//----------------------------------------------------------------- PUBLIC
public:
//----------------------------------------------------- Méthodes publiques
    int Menu() const;
    // Mode d'emploi :
    // affichage du menu de l'application et saisie du choix
    // Contrat :
    // la saisie doit etre un nombre

    void NouveauTrajet();
    // Mode d'emploi :
    // creation et ajout d'un nouveau trajet avec des saisies au clavier
    // Contrat :
    // les saisies ne doivent pas depasser 40 caractères (41 avec \0)

    void Afficher() const;
    // Mode d'emploi :
    // affichage des caracteristiques de chaque trajet

    void Rechercher() const;
    // Mode d'emploi :
    // recherche d'un trajet avec saisies au clavier

    void Sauvegarder() const;
    // Mode d'emploi :
    // sauvegarde de trajets dans un fichier externe

    void Charger();
    // Mode d'emploi :
    // ajout de trajets depuis un fichier externe

//-------------------------------------------- Constructeurs - destructeur
    Catalogue(const Catalogue & ctl);
    // Contrat :
    // pas d'utilisation, utile pour debuggage

    Catalogue();
    // Mode d'emploi :
    // initialise le catalogue avec une liste chainee vide

    virtual ~Catalogue();

//------------------------------------------------------------------ PRIVE
protected:
//----------------------------------------------------- Méthodes protégées
    void rechercheAvancee(ListeChainee* marque, ListeChainee* parcours, char* arrivee) const;
    // Mode d'emploi :
    // realise la recherche avancee en utilisant un parcours de graphe en profondeur

    void insereTrajet(Element* elem);
    // Mode d'emploi :
    // insere un trajet dans le catalogue a la bonne position selon une regle
    // de tri par ordre alphabétique des villes de départ puis d'arrivée
    // Contrat :
    // chaque trajet a ajouter au catalogue doit etre insere avec cette methode
    // pour respecter la contrainte d'ordonnancement

    int saisieEntier() const;
    // Mode d'emploi :
    // saisie d'un entier avec gestion des erreur saisie (mauvais type)

//----------------------------------------------------- Attributs protégés
    static const int TAILLE_CHAINE = 41;
    ListeChainee* liste;
};

#endif // CATALOGUE_H
/*************************************************************************
                           ListeChainee  -  description
                             -------------------
    début                : 21/11/2022
    copyright            : (C) 2022 par Arthur Galet, Alexandre Rosard
    e-mail               : arthur.galet@insa-lyon.fr, alexandre.rosard@insa-lyon.fr
*************************************************************************/

//---------- Réalisation de la classe ListeChainee (fichier ListeChainee.cpp) ------------

//---------------------------------------------------------------- INCLUDE
//-------------------------------------------------------- Include système
using namespace std;
#include <iostream>

//------------------------------------------------------ Include personnel
#include "ListeChainee.h"
#include "Element.h"

//----------------------------------------------------------------- PUBLIC
//----------------------------------------------------- Méthodes publiques

void ListeChainee::InsererDebut(Element* elem)
{
    elem->MettreSuivant(tete);
    tete = elem;
    if (queue == nullptr) { // file vide
        queue = elem;
    }

} //----- Fin de Méthode InsererFin

void ListeChainee::InsererFin(Element* elem)
{
    if (tete == nullptr) { // file vide
        tete = elem;
    } else {
        queue->MettreSuivant(elem);
    }
    queue = elem;

} //----- Fin de Méthode InsererFin


Element* ListeChainee::ObtenirTete() const
{
    return tete;
} //----- Fin de Méthode ObtenirTete

Element* ListeChainee::ObtenirQueue() const
{
    return queue;
} //----- Fin de Méthode ObtenirQueue


void ListeChainee::MettreTete(Element* elem)
{
    tete = elem;
} //----- Fin de Méthode MettreTete


void ListeChainee::MettreQueue(Element* elem)
{
    queue = elem;
} //----- Fin de Méthode MettreQueue

//-------------------------------------------- Constructeurs - destructeur
ListeChainee::ListeChainee(const ListeChainee & lc)
{
#ifdef MAP
    cout << "Appel au constructeur de copie ListeChainee" << endl;
#endif
} //----- Fin de ListeChainee (construceur de copie)

ListeChainee::ListeChainee()
{
    tete = nullptr;
    queue = nullptr;

#ifdef MAP
    cout << "Appel au constructeur de ListeChainee" << endl;
#endif
} //----- Fin de ListeChainee


ListeChainee::~ListeChainee()
{
#ifdef MAP
    cout << "Appel au destructeur de ListeChainee" << endl;
#endif
    Element* elem = tete;
    Element* precedant;
    while(elem != nullptr) {
        delete elem->ObtenirTrajet();
        precedant = elem;
        elem = elem->ObtenirSuivant();
        delete precedant;
    }
} //----- Fin de ~ListeChainee
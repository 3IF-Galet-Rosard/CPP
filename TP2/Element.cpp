/*************************************************************************
                           Element.cpp  -  description
                             -------------------
    début                : 21/11/2022
    copyright            : (C) 2022 par Arthur Galet, Alexandre Rosard
    e-mail               : arthur.galet@insa-lyon.fr, alexandre.rosard@insa-lyon.fr
*************************************************************************/

//---------- Réalisation de la classe Element (fichier Element.cpp) ------------

//---------------------------------------------------------------- INCLUDE
//-------------------------------------------------------- Include système
using namespace std;
#include <iostream>
//------------------------------------------------------ Include personnel
#include "Element.h"
#include "Trajet.h"

//----------------------------------------------------------------- PUBLIC
//----------------------------------------------------- Méthodes publiques
Element* Element::ObtenirSuivant()
{
    return suivant;
} //----- Fin de Méthode ObtenirSuivant()

void Element::MettreSuivant(Element* elem)
{
    suivant = elem;
} //----- Fin de Méthode MettreSuivant()

Trajet* Element::ObtenirTrajet()
{
    return trajet;
} //----- Fin de Méthode ObtenirTrajet()

//-------------------------------------------- Constructeurs - destructeur
Element::Element(const Element & elem)
{
#ifdef MAP
    cout << "Appel au constructeur de copie de Element" << endl;
#endif
} //----- Fin de Element (constructeur de copie)

Element::Element(Trajet* trj)
{
#ifdef MAP
    cout << "Appel au constructeur de Element" << endl;
#endif
    trajet = trj;
    suivant = nullptr;
} //----- Fin de Element

Element::~Element()
{
#ifdef MAP
    cout << "Appel au destructeur de Element" << endl;
#endif
} //----- Fin de ~Element



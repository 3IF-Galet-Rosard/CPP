/*************************************************************************
                           Trajet  -  description
                             -------------------
    début                : 21/11/2022
    copyright            : (C) 2022 par Arthur Galet, Alexandre Rosard
    e-mail               : arthur.galet@insa-lyon.fr, alexandre.rosard@insa-lyon.fr
*************************************************************************/

//---------- Réalisation de la classe <Trajet> (fichier Trajet.cpp) ------------

//---------------------------------------------------------------- INCLUDE
//-------------------------------------------------------- Include système
using namespace std;
#include <iostream>
//------------------------------------------------------ Include personnel
#include "Trajet.h"

//----------------------------------------------------------------- PUBLIC
//-------------------------------------------- Constructeurs - destructeur
Trajet::Trajet ( const Trajet & unTrajet )
{
#ifdef MAP
    cout << "Appel au constructeur de copie de Trajet" << endl;
#endif
} //----- Fin de Trajet (constructeur de copie)

Trajet::Trajet ()
{
#ifdef MAP
    cout << "Appel au constructeur de Trajet" << endl;
#endif

} //----- Fin de Trajet

Trajet::~Trajet()
{
#ifdef MAP
    cout << "Appel au destructeur de Trajet" << endl;
#endif
} //----- Fin de ~Trajet
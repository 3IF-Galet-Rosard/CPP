/*************************************************************************
                           ListeChainee  -  description
                             -------------------
    début                : 21/11/2022
    copyright            : (C) 2022 par Arthur Galet, Alexandre Rosard
    e-mail               : arthur.galet@insa-lyon.fr, alexandre.rosard@insa-lyon.fr
*************************************************************************/

//---------- Interface de la classe <ListeChainee> (fichier ListeChainee.h) ----------------

#if ! defined ( LISTECHAINEE_H )
#define LISTECHAINEE_H

//--------------------------------------------------- Interfaces utilisées
#include "Element.h"

//------------------------------------------------------------------------
// Rôle de la classe <ListeChainee>
// classe métier permettant de stocker des trajets dans une
// liste chaine indépendamment de l'usage de la liste
//------------------------------------------------------------------------


class ListeChainee
{
//----------------------------------------------------------------- PUBLIC
public:
//----------------------------------------------------- Méthodes publiques

    void InsererDebut(Element* elem);
    // Mode d'emploi :
    // insere un element avant le premier element actuel de la liste

    void InsererFin(Element* elem);
    // Mode d'emploi :
    // insere un element après le dernier element actuel de la liste

    Element* ObtenirTete() const;
    // Mode d'emploi :
    // retourne le premier element de la liste

    Element* ObtenirQueue() const;
    // Mode d'emploi :
    // retourne le dernier element de la liste

    void MettreTete(Element*);
    // Mode d'emploi :
    // remplace l'attribut tete par le pointeur sur element passe en parametre


    void MettreQueue(Element*);
    // Mode d'emploi :
    // remplace l'attribut queue par le pointeur sur element passe en parametre

//-------------------------------------------- Constructeurs - destructeur
    ListeChainee(const ListeChainee & lc);
    // Contrat :
    // pas d'utilisation, utile pour debuggage

    ListeChainee();
    // Mode d'emploi :
    // construit une liste chainee vide dont la tete et la queue sont
    // initialisees avec des pointeurs nuls

    virtual ~ListeChainee();

//------------------------------------------------------------------ PRIVE
protected:
//----------------------------------------------------- Attributs protégés
    Element* tete;
    Element* queue;

};

#endif // LISTECHAINEE_H


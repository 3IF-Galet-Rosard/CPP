/*************************************************************************
                           Element.h  -  description
                             -------------------
    début                : 21/11/2022
    copyright            : (C) 2022 par Arthur Galet, Alexandre Rosard
    e-mail               : arthur.galet@insa-lyon.fr, alexandre.rosard@insa-lyon.fr
*************************************************************************/

//---------- Interface de la classe Element (fichier Element.h) ----------------
#if ! defined ( ELEMENT_H )
#define ELEMENT_H

//--------------------------------------------------- Interfaces utilisées
#include "Trajet.h"
//------------------------------------------------------------------------
// Rôle de la classe Element
// modélise un élément appartenant à une liste chaînée
// un élément est composé d'une donnée (un pointeur sur un trajet) et d'un
// pointeur sur l'élément suivant
//------------------------------------------------------------------------

class Element
{
//----------------------------------------------------------------- PUBLIC
public:
//----------------------------------------------------- Méthodes publiques
    Element* ObtenirSuivant();
    // Mode d'emploi :
    // retourne l'element suivant l'element courant

    void MettreSuivant(Element* elem);
    // Mode d'emploi :
    // remplace l'attribut suivant par le pointeur sur element passe en parametre

    Trajet* ObtenirTrajet();
    // Mode d'emploi :
    // retourne le trajet associe a l'element courant

//-------------------------------------------- Constructeurs - destructeur
    Element(const Element & elem);
    // Contrat :
    // pas d'utilisation, utile pour debuggage

    Element(Trajet*);
    // Mode d'emploi :
    // construit un nouvel élément avec
    // un pointeur sur un trajet passe en parametre
    // un element suivant nul

    virtual ~Element();

//------------------------------------------------------------------ PRIVE
protected:
//----------------------------------------------------- Attributs protégés
    Trajet* trajet;
    Element* suivant;
};

#endif // ELEMENT_H


/*************************************************************************
                           Ensemble  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Réalisation de la classe <Ensemble> (fichier Ensemble.cpp) ------------

//---------------------------------------------------------------- INCLUDE

//-------------------------------------------------------- Include système
#include <iostream>
using namespace std;

//------------------------------------------------------ Include personnel
#include "Ensemble.h"

//------------------------------------------------------------- Constantes

//----------------------------------------------------------------- PUBLIC

//----------------------------------------------------- Méthodes publiques
// type Ensemble::Méthode ( liste des paramètres )
// Algorithme :
//
//{
//} //----- Fin de Méthode


//-------------------------------------------- Constructeurs - destructeur
Ensemble::Ensemble ( const Ensemble & unEnsemble )
// Algorithme :
//
{
#ifdef MAP
    cout << "Appel au constructeur de copie de <Ensemble>" << endl;
#endif
} //----- Fin de Ensemble (constructeur de copie)

Ensemble::Ensemble(int t[], unsigned int nbElements) {
    cardMax = nbElements;
    cardAct = 0;
    if (nbElements == 0) {
        tableau = NULL;
    } else {
        tableau = new int[nbElements];

        unsigned int i, j;
        bool present;

        for (i = 0; i < nbElements; i++) {
            present = false;
            j = 0;

            while (j < cardAct and not present) {
                present = t[i] == tableau[j];
                j++;
            }

            if (not present) {
                tableau[cardAct] = t[i];
                cardAct++;
            }
        }
    }
}

Ensemble::Ensemble (unsigned int card)
// Algorithme :
//
{
    cardMax = card;
    cardAct = 0;

    if (cardMax != 0) {
        tableau = new int[cardMax];
    } else {
        tableau = NULL;
    }
#ifdef MAP
    cout << "Appel au constructeur de <Ensemble>" << endl;
#endif
} //----- Fin de Ensemble


Ensemble::~Ensemble ( )
// Algorithme :
//
{
#ifdef MAP
    cout << "Appel au destructeur de <Ensemble>" << endl;
#endif
} //----- Fin de ~Ensemble

void Ensemble::trier() const
{
    unsigned int i, j, jmin;
    int min, tmp;

    for (i=0; i<cardAct; i++) {
        min = tableau[i];
        jmin = i;
        for (j=i + 1; j<cardAct; j++) {
            if (tableau[j] < min) {
                min = tableau[j];
                jmin = j;
            }
        }
        tmp = tableau[i];
        tableau[i] = tableau[jmin];
        tableau[jmin] = tmp;
    }
}

bool Ensemble::EstEgal(const Ensemble & unEnsemble) const
{
    trier();
    unEnsemble.trier();
    bool egal = cardAct == unEnsemble.cardAct;
    unsigned int i = 0;

    while (egal and i<cardAct) {
        egal = tableau[i] == unEnsemble.tableau[i];
        i++;
    }

    return egal;
}

crduEstInclus Ensemble::EstInclus(const Ensemble & unEnsemble) const
{
    int i = 0, j = 0;
    crduEstInclus inclus = INCLUSION_LARGE;

    trier();
    unEnsemble.trier();

    while (i < cardAct and j < unEnsemble.cardAct) {
        if (tableau[i] == unEnsemble.tableau[j]) {
            i++;
            j++;
        } else if (tableau[i] > unEnsemble.tableau[j]) {
            j++;
        } else {
            inclus = NON_INCLUSION;
            break;
        }
    }

    if (inclus == INCLUSION_LARGE and not EstEgal(unEnsemble)) {
        inclus = INCLUSION_STRICTE;
    }

    return inclus;
}

void Ensemble::Afficher()
{
    unsigned int i;
    trier();

    cout << cardAct << "\r\n" << cardMax << "\r\n{";
    for (i=0; i<cardAct; i++) {
        if (i >= 1) {
            cout << ",";
        }
        cout << tableau[i];
    }
    cout << "}\r\n";
}

//------------------------------------------------------------------ PRIVE

//----------------------------------------------------- Méthodes protégées


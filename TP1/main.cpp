//
// Created by galet on 24/10/2022.
//
#include <iostream>
using namespace std;

#include "Ensemble.h"
int main() {
    int tab1[] = {3, 4, 3, 1, 7, 3};
    int tab2[] = {1, 3, 3, 7, 3, 4};
    int tab3[] = {1, 7, 3};
    Ensemble e1(tab1, 6);
    Ensemble e2(tab2, 6);
    Ensemble e3(tab3, 3);
    e1.Afficher();
    e2.Afficher();
    e3.Afficher();
    cout << e1.EstInclus(e2);
    cout << e1.EstEgal(e3);
    cout << e3.EstEgal(e2);
}
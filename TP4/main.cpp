using namespace std;

#include <iostream>
#include <fstream>
#include <tuple>
#include <unordered_map>
#include <map>

#include "EntreeLog.h"

const string nomFichierURLBASE = "./BASE_URL";

int main(int argc, char* argv[]) {

    // ----- GESTION INPUT UTILISATEUR -----//

    string usage = "Usage : ./analog [-g nom fichier optGraphe] [-e] [-t heure] fichier log";
    if (argc < 2) {
        cout << usage << endl;
        return 1;
    }

    int nbArg = 1, temps = -1;
    bool optGraphe = false, optExclure = false, optTemps = false, fichierChoisi = false;
    string nomFichierLog, nomFichierGraphe;

    while (nbArg < argc) {
        string opt(argv[nbArg]);

        if (opt == "-g") {
            if (optGraphe) {
                cout << "L'option -g ne peut etre utilisee qu'une seule fois" << endl << usage << endl;
                return 2;
            }

            optGraphe = true;

            if (nbArg + 1 == argc) { // pas de suite
                cout << "L'option -g a besoin d'un parametre suplementaire" << endl << usage << endl;
                return 3;

            } else {
                ++nbArg;
                nomFichierGraphe = string(argv[nbArg]) ;
            }

        } else if (opt == "-e") {
            if (optExclure) {
                cout << "L'option -e ne peut etre utilisee qu'une seule fois" << endl << usage << endl;
                return 4;
            }

            optExclure = true;

        } else if (opt == "-t") {
            if (optTemps) {
                cout << "L'option -t ne peut etre utilisee qu'une seule fois" << endl << usage << endl;
                return 5;
            }

            optTemps = true;

            if (nbArg + 1 == argc) { // pas de suite
                cout << "L'option -t a besoin d'un parametre suplementaire" << endl << usage << endl;
                return 6;

            } else {
                try {
                    ++nbArg;
                    temps = stoi(argv[nbArg]);

                    if (temps < 0 or temps > 23) {
                        cout << "Le creneau precise n'est pas compris entre 0 et 23" << endl << usage << endl;
                        return 7;
                    }

                } catch (invalid_argument e) {
                    cout << "Le creneau precise est invalide" << endl << usage << endl;
                    return 8;

                } catch (out_of_range e) {
                    cout << "Le creneau precise n'est pas compris entre 0 et 23" << endl << usage << endl;
                    return 9;
                }

            }
        } else {
            if (fichierChoisi) {
                cout << "Parametre invalide : " << argv[nbArg] << endl << usage << endl;
                return 10;
            }

            fichierChoisi = true;
            nomFichierLog = string(argv[nbArg]);
        }

        ++nbArg;
    }

    if (not fichierChoisi) {
        cout << "Aucun fichier de log specifie " << endl << usage << endl;
        return 11;
    }


    // ----- COEUR DE L'APPLICATION -----//

    ifstream fichierURL(nomFichierURLBASE);
    string URL_BASE;
    if (fichierURL.good() and getline(fichierURL, URL_BASE)) {
        if (not URL_BASE.empty()) {
            EntreeLog::MettreURLbase(URL_BASE);
        }
    }

    ifstream fichier(nomFichierLog);
    unordered_map<string,tuple<int, unordered_map<string, int>>> mapPagesCibles;
    EntreeLog* el;
    string ligne;
    bool nouveau;

    // lecture du fichier log
    while (not fichier.eof() and getline(fichier, ligne)) {

        // lecture de l'entree
        el = new EntreeLog(ligne);
        nouveau = false;

        // exclusion des images, fichiers javascript et css
        if (optExclure and (
                    el->ObtenirCible().find(".css") != string::npos or
                    el->ObtenirCible().find(".js") != string::npos or
                    el->ObtenirCible().find(".ico") != string::npos or
                    el->ObtenirCible().find(".png") != string::npos or
                    el->ObtenirCible().find(".jpg") != string::npos or
                    el->ObtenirCible().find(".jpeg") != string::npos or
                    el->ObtenirCible().find(".gif") != string::npos or
                    el->ObtenirCible().find(".bmp") != string::npos or
                    el->ObtenirCible().find(".ico") != string::npos
                )) {
            continue;
        }

        // exclusion des entrees a la mauvaise heure
        if (optTemps and temps != (el->ObtenirHeureEnSecondes() / 3600)) {
            continue;
        }

        // ----- Gestion des pages cibles ----- //

        // si la page cible n'est pas une cle de la map
        if (mapPagesCibles.count(el->ObtenirCible()) == 0) {
            nouveau = true;

            // insertion d'une entree
            unordered_map<string, int> mapPagesReferer;
            mapPagesReferer.insert(pair<string, int>(el->ObtenirReferer(), 1));

            tuple<int, unordered_map<string, int>> tup(1, mapPagesReferer);

            mapPagesCibles.insert(pair<string, tuple<int, unordered_map<string, int>>>(el->ObtenirCible(), tup));

        } else {
        // si la page cible est une cle de la map

            // ajout d'un accces supplementaire au compteur
            ++get<0>((mapPagesCibles[el->ObtenirCible()]));
        }

        // ----- Gestion des pages referers ----- //

        // si l'entree de la page cible vient d'etre creee
        // ou si la page referer n'est pas une cle dans la map de la page cible
        if (nouveau or get<1>(mapPagesCibles[el->ObtenirCible()]).count(el->ObtenirReferer()) == 0) {

            // ajout de la page referer dans la map de la page cible
            get<1>(mapPagesCibles[el->ObtenirCible()]).insert(pair<string, int>(el->ObtenirReferer(), 1));

        } else {
        // si la page referer est deja une cle dans la map de la page cible

            // ajout d'un acces supplementaire au compteur
            ++(get<1>(mapPagesCibles[el->ObtenirCible()])[el->ObtenirReferer()]);
        }

        delete el;
    }


    // ----- TOP 10 ----- //

    int i, j;
    array<tuple<string, int>, 10> tab;
    for (i=0; i<10; ++i) {
        tab[i] = tuple <string, int>{"", 0};
    }

    unordered_map <string, tuple <int, unordered_map < string, int >>>::iterator itPagesCibles;

    for (itPagesCibles = mapPagesCibles.begin(); itPagesCibles != mapPagesCibles.end(); ++itPagesCibles) {

        // comparaison de la page courante avec la derniere page du top 10
        if (get<0>((itPagesCibles->second)) > get<1>(tab[9])) {

            // ajout de la page a la 10 eme place
            tab[9] = tuple <string, int>{itPagesCibles->first, get<0>((itPagesCibles->second))};

            j = 9; // indice de la place a laquelle la nouvelle page correspond dans le top
            while (j > 0 and get<0>((itPagesCibles->second)) > get<1>(tab[j - 1])) { // tri bulle

                // permutation
                tuple <string, int> tmp = tab[j];
                tab[j] = tab[j - 1];
                tab[j - 1] = tmp;
                --j;
            }
        }
    }

    // Affichage TOP 10
    cout << " --- TOP 10 --- " << endl;
    for (i=0; i<10; ++i) {
        cout << i + 1 << " - " << get<0>(tab[i]) << " : " << get<1>(tab[i])<< endl;
    }


        // ----- Gestion du fichier graphe ----- //

    if (optGraphe) {
        ofstream fichierGraphe(nomFichierGraphe);
        fichierGraphe << "digraph {" << endl;

        unordered_map < string, int >::iterator itPagesReferers;

        for (itPagesCibles = mapPagesCibles.begin(); itPagesCibles != mapPagesCibles.end(); ++itPagesCibles) {
            for (itPagesReferers = get<1>(itPagesCibles->second).begin(); itPagesReferers != get<1>(itPagesCibles->second).end();  ++itPagesReferers) {
                fichierGraphe << "\"" << itPagesReferers->first << "\" -> \"" << itPagesCibles->first << "\" [label=\"" << itPagesReferers->second << "\"];" << endl;
            }
        }

        fichierGraphe << "}";
        fichierGraphe.close();
    }

    fichier.close();

    return 0;
}




/*************************************************************************
                           EntreeLog  -  description
                             -------------------
    début                : 16/01/2023
    copyright            : (C) 2022 par Arthur Galet, Alexandre Rosard
    e-mail               : arthur.galet@insa-lyon.fr, alexandre.rosard@insa-lyon.fr
*************************************************************************/

//---------- Réalisation de la classe <EntreeLog> (fichier EntreeLog.cpp) ------------

//---------------------------------------------------------------- INCLUDE
//-------------------------------------------------------- Include système
using namespace std;
//------------------------------------------------------ Include personnel
#include "EntreeLog.h"

//------------------------------------------------------------- Constantes
string EntreeLog::BASE_URL = "https://tmp";
//----------------------------------------------------------------- PUBLIC
//----------------------------------------------------- Méthodes publiques

void EntreeLog::Afficher() const
{
    cout << "ip: " << ip << endl;
    cout << "visiteur: " << nomVisiteur << endl;
    cout << "utilisateur: " << nomUtilisateur << endl;
    cout << "date: " << date << endl;
    cout << "heure (en secondes): " << heureEnSecondes << endl;
    cout << "fuseau horaire: " << fuseauHoraire << endl;
    cout << "methode: " << methode << endl;
    cout << "cible: " << cible << endl;
    cout << "protocole: " << protocole << endl;
    cout << "statut: " << statut << endl;
    cout << "quantite de donnees: " << quantiteDonnees << endl;
    cout << "referer: " << referer << endl;
    cout << "navigateur: " << navigateur << endl;
}

string EntreeLog::ObtenirURLbase() const
{
    return BASE_URL;
} //----- Fin de ObtenirURLbase

string EntreeLog::ObtenirIp() const
{
    return ip;
} //----- Fin de ObtenirIp


string EntreeLog::ObtenirNomVisiteur() const{
    return nomVisiteur;
} //----- Fin de ObtenirNomVisiteur


string EntreeLog::ObtenirNomUtilisateur() const{
    return nomUtilisateur;
} //----- Fin de ObtenirNomUtilisateur

string EntreeLog::ObtenirDate() const
{
    return date;
} //----- Fin de ObtenirDate


int EntreeLog::ObtenirHeureEnSecondes() const
{
    return heureEnSecondes;
} //----- Fin de ObtenirHeure


string EntreeLog::ObtenirFuseauHoraire() const
{
    return fuseauHoraire;
} //----- Fin de ObtenirFuseauHoraire


string EntreeLog::ObtenirMethode() const
{
    return methode;
} //----- Fin de ObtenirMethode


string EntreeLog::ObtenirCible() const
{
    return cible;
} //----- Fin de ObtenirCible


string EntreeLog::ObtenirProtocole() const
{
    return protocole;
} //----- Fin de ObtenirProtocole


int EntreeLog::ObtenirStatut() const
{
    return statut;
} //----- Fin de ObtenirStatut


int EntreeLog::ObtenirQuantiteDonnees() const
{
    return quantiteDonnees;
} //----- Fin de ObtenirQuantiteDonnees


string EntreeLog::ObtenirReferer() const
{
    return referer;
} //----- Fin de ObtenirReferer


string EntreeLog::ObtenirNavigateur() const
{
    return navigateur;
} //----- Fin de ObtenirNavigateur


void EntreeLog::MettreURLbase(string URL)
{
    BASE_URL = URL;
} //----- Fin de MettreURLbase

//-------------------------------------------- Constructeurs - destructeur
EntreeLog::EntreeLog ( const EntreeLog & unEntreeLog )
{
#ifdef MAP
    cout << "Appel au constructeur de copie de <EntreeLog>" << endl;
#endif
} //----- Fin de EntreeLog (constructeur de copie)


EntreeLog::EntreeLog (string ligneLog)
{
#ifdef MAP
    cout << "Appel au constructeur de <EntreeLog>" << endl;
#endif
    // Creation du flux de string
    istringstream issLog(ligneLog);
    string champ;

    getline(issLog, champ, ' ');
    ip = champ;

    getline(issLog, champ, ' ');
    nomVisiteur = champ;

    getline(issLog, champ, ' ');
    nomUtilisateur = champ;

    // date, heure
    getline(issLog, champ, ' ');
    date = champ.substr(1, champ.find(":") - 1);

    champ.erase(0, champ.find(":") + 1);
    heureEnSecondes = stoi(champ.substr(0, champ.find(":"))) * 3600;
    champ.erase(0, champ.find(":") + 1);
    heureEnSecondes += stoi(champ.substr(0, champ.find(":"))) * 60;
    champ.erase(0, champ.find(":") + 1);
    heureEnSecondes += stoi(champ.substr(0, champ.find(" ")));
    getline(issLog, champ, ' ');
    fuseauHoraire = champ.substr(0, champ.find("]"));

    getline(issLog, champ, ' ');
    methode = champ.substr(1, champ.length() - 1);

    // page cible et protocole
    getline(issLog, champ, '"');
    cible = champ.substr(0, champ.find_last_of(' '));
    cible = cible.substr(0, cible.find("?"));
    cible = cible.substr(0, cible.find(";"));

    if (cible.find(BASE_URL) != string::npos) {
        cout << referer;
        cible = cible.substr(cible.find(BASE_URL) + BASE_URL.length(), cible.length() - 1);
        cout << " - " << referer << endl;
    }

    if (cible.back() == '/') {
        cible = cible.substr(0,cible.length() - 1);
    }
    protocole = champ.substr(champ.find_last_of(" ") + 1);

    getline(issLog, champ, ' ');
    getline(issLog, champ, ' ');
    statut = stoi(champ);

    getline(issLog, champ, ' ');
    if (champ == "-") {
        quantiteDonnees = 0;
    } else {
        quantiteDonnees = stoi(champ);
    }

    getline(issLog, champ, '"');
    getline(issLog, champ, '"');
    referer = champ.substr(0, champ.length());
    referer = referer.substr(0, referer.find("?"));
    referer = referer.substr(0, referer.find(";"));

    if (referer.find(BASE_URL) != string::npos) {
        referer = referer.substr(referer.find(BASE_URL) + BASE_URL.length(), referer.length() - 1);
    }

    if (referer.back() == '/') {
        referer = referer.substr(0,referer.length() - 1);
    }

    getline(issLog, champ, '"');
    getline(issLog, champ, '"');
    navigateur = champ.substr(0, champ.length());

} //----- Fin de EntreeLog


EntreeLog::~EntreeLog ( )
{
#ifdef MAP
    cout << "Appel au destructeur de <EntreeLog>" << endl;
#endif
} //----- Fin de ~EntreeLog


/*************************************************************************
                           EntreeLog  -  description
                             -------------------
    début                : 16/01/2022
    copyright            : (C) 2022 par Arthur Galet, Alexandre Rosard
    e-mail               : arthur.galet@insa-lyon.fr, alexandre.rosard@insa-lyon.fr
*************************************************************************/

//---------- Interface de la classe <EntreeLog> (fichier EntreeLog.h) ----------------
#if ! defined ( EntreeLog_H )
#define EntreeLog_H

//--------------------------------------------------- Interfaces utilisées
using namespace std;
#include <string>
#include <iostream>
#include <sstream>

//------------------------------------------------------------------------
// Rôle de la classe <EntreeLog>
// modelisation d'une entree de log de serveur web Apache
// avec un constructeur utilisant une ligne de log et des attributs
// correspondants aux champs de cette ligne
//------------------------------------------------------------------------

class EntreeLog
{
//----------------------------------------------------------------- PUBLIC

public:
//----------------------------------------------------- Méthodes publiques

    void Afficher() const;
    // MOde d'emploi :
    // Affiche l'ensemble des attributs

    string ObtenirURLbase() const;
    // Mode d'emploi :
    // Retourne l'adresse IP

    string ObtenirIp() const;
    // Mode d'emploi :
    // Retourne l'adresse IP

    string ObtenirNomVisiteur() const;
    // Mode d'emploi :
    // Retourne le nom d'utilisateur du visiteur

    string ObtenirNomUtilisateur() const;
    // Mode d'emploi :
    // Retourne le nom d'utilisateur de l'internaute

    string ObtenirDate() const;
    // Mode d'emploi :
    // Retourne la date

    int ObtenirHeureEnSecondes() const;
    // Mode d'emploi :
    // Retourne l'heure relative

    string ObtenirFuseauHoraire() const;
    // Mode d'emploi :
    // Retourne le fuseau horaire

    string ObtenirMethode() const;
    // Mode d'emploi :
    // Retourne la methode

    string ObtenirCible() const;
    // Mode d'emploi :
    // Retourne l'URL de la cible

    string ObtenirProtocole() const;
    // Mode d'emploi :
    // Retourne le protocole

    int ObtenirStatut() const;
    // Mode d'emploi :
    // Retourne le statut

    int ObtenirQuantiteDonnees() const;
    // Mode d'emploi :
    // Retourne la quantite de donnees transferee

    string ObtenirReferer() const;
    // Mode d'emploi :
    // Retourne l'URL du referer

    string ObtenirNavigateur() const;
    // Mode d'emploi :
    // Retourne le navigateur

    static void MettreURLbase(string URL);
    // Mode d'emploi :
    // Modifie l'URLbase avec l'URL passée en paramètre

//-------------------------------------------- Constructeurs - destructeur
    EntreeLog ( const EntreeLog & unEntreeLog );
    // Mode d'emploi (constructeur de copie) :
    //
    // Contrat :
    //

    EntreeLog (const string ligneLog);
    // Mode d'emploi :
    //
    // Contrat :
    //

    virtual ~EntreeLog ( );
    // Mode d'emploi :
    //
    // Contrat :
    //

//------------------------------------------------------------------ PRIVE
protected:

//----------------------------------------------------- Attributs protégés
    static string BASE_URL;

    string ip;
    string nomVisiteur;
    string nomUtilisateur;

    string date;
    int heureEnSecondes;
    string fuseauHoraire;

    string methode;
    string cible;
    string protocole;
    int statut;
    int quantiteDonnees;
    string referer;
    string navigateur;

};

#endif // EntreeLog_H

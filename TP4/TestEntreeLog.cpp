/*************************************************************************
                           TestEntreeLog  -  description
                             -------------------
    début                : 30/01/2023
    copyright            : (C) 2022 par Arthur Galet, Alexandre Rosard
    e-mail               : arthur.galet@insa-lyon.fr, alexandre.rosard@insa-lyon.fr
*************************************************************************/

//---------- Réalisation du module <TestEntreeLog> (fichier TestEntreeLog.cpp) ---------------

using namespace std;

/////////////////////////////////////////////////////////////////  INCLUDE
//-------------------------------------------------------- Include système
#include <fstream>

//-------------------------------------------------------- Include système
#include "EntreeLog.h"

///////////////////////////////////////////////////////////////////  PRIVE

//------------------------------------------------------ Fonctions privées

string lectureEntree (ifstream & in)
// Mode d'emploi :
//
// Contrat :
//
// Algorithme :
//
{
    string ligne, retour;
    getline(in, ligne);

    EntreeLog e(ligne);

    retour = e.ObtenirIp() + " ";
    retour += e.ObtenirNomVisiteur() + " ";
    retour += e.ObtenirNomUtilisateur() + " [";
    retour += e.ObtenirDate() + ":";
    if ((e.ObtenirHeureEnSecondes() / 3600) < 10)
        retour += to_string(0);
    retour += to_string(e.ObtenirHeureEnSecondes() / 3600) + ":";
    if (((e.ObtenirHeureEnSecondes() % 3600) / 60) < 10)
        retour += to_string(0);
    retour += to_string((e.ObtenirHeureEnSecondes() % 3600 / 60)) + ":";
    if ((e.ObtenirHeureEnSecondes() % 60) < 10)
        retour += to_string(0);
    retour += to_string(e.ObtenirHeureEnSecondes() % 60) + " ";
    retour += e.ObtenirFuseauHoraire() + "] \"";
    retour += e.ObtenirMethode() + " ";
    retour += e.ObtenirCible() + " ";
    retour += e.ObtenirProtocole() + "\" ";
    retour += to_string(e.ObtenirStatut()) + " ";
    retour += to_string(e.ObtenirQuantiteDonnees()) + " \"";
    retour += e.ObtenirReferer() + "\" \"";
    retour += e.ObtenirNavigateur() + "\"";

    return retour;
}

//////////////////////////////////////////////////////////////////  PUBLIC
//---------------------------------------------------- Fonctions publiques

int main() {
    ifstream in("in.log");
    while (not in.eof())
        cout << lectureEntree(in) << endl;
    in.close();
}